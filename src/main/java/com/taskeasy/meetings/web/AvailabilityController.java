package com.taskeasy.meetings.web;


import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.taskeasy.meetings.models.Disposition;
import com.taskeasy.meetings.models.Person;
import com.taskeasy.meetings.data.MockData;
import com.taskeasy.meetings.calcs.Availability;;

@RestController
public class AvailabilityController {
	
	//// Office hours 8AM to 5PM
	//// Lunch is 12 PM - 1PM
	//// Number of people is 6
	//// Meetings are half an hour long
	//// 4 hours in the morning and 4 hours in the afternoon
	//// each meeting is 30 minutes long 
	/// 12:00M(noon) and 5:00PM not consider meeting hours as it would collide with working hour restrictions if you start at those times.
	
	//Returns a JSON  for easy of use for another program
	// on browser or GET request to: http://localhost:8080/availability
	@RequestMapping(value = "/availability", method = RequestMethod.GET)
	public Iterable<Disposition> respondHello() throws ParseException {

	
		MockData data = new MockData();
		Availability avaliability = new Availability();
		
		ArrayList<Person> persons = data.getPersons();
		ArrayList<Date> segments = data.getWorkingDaySegments();
		
		ArrayList<Disposition> dispositions =  avaliability.getAvailability(segments, persons);

		return dispositions;
		 
	}
	

	
}
