package com.taskeasy.meetings.calcs;

import java.util.ArrayList;
import java.util.Date;

import com.taskeasy.meetings.models.Disposition;
import com.taskeasy.meetings.models.Person;

public class Availability {

public  ArrayList<Disposition> getAvailability (ArrayList<Date> segments, ArrayList<Person> people) {
		
		ArrayList<Disposition> availabiliest = new ArrayList<Disposition>();
		
		//Iterate each time segment
		for (Date segment : segments) {
			Disposition disp = new Disposition(segment.toString().substring(11, 19));
			// Iterate each person
			for (Person person : people) {
				int accumulatorIfNoMeetings = 0;
				// Iterate through a person meetings for the day
				for (Date meeting : person.getDates()) {
				// If the person has a meeting at that time
					if (segment.compareTo(meeting) == 0) {
						break;
					}
					// Increase to compare at the end of iteration if final meeting reached.
					accumulatorIfNoMeetings += 1;
					// If this is true then it means that this person has no more meetings at that segment			
					if (accumulatorIfNoMeetings == person.getDates().size()) {
						disp.addToAvailablePeople(person.getName());
					}
				}
			}
			// Check if at the end of inner iterations there is enough people available for
			// the given segment
			if (disp.getAvailablePeople().size() >= 3)
				availabiliest.add(disp);
		}

		return availabiliest;
	}
}
