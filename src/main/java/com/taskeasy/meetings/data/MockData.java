package com.taskeasy.meetings.data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import com.taskeasy.meetings.models.Person;

public class MockData {
	
	final static SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");

	
	public  ArrayList<Date> getWorkingDaySegments() throws ParseException {
		
		Calendar cl = Calendar.getInstance();
		Date startDay = dateformat.parse("8:00");
		Date endDay = dateformat.parse("17:00");
		Date startLunch = dateformat.parse("12:00");
		Date endLunch = dateformat.parse("13:00");

		cl.setTime(startDay);
		ArrayList<Date> segments = new ArrayList<>();

		while (cl.getTime().compareTo(endDay) < 0) {

			if (cl.getTime().compareTo(startDay) >= 0 && cl.getTime().compareTo(endDay) < 0) {
				if (cl.getTime().compareTo(startLunch) < 0 || cl.getTime().compareTo(endLunch) >= 0) {

					segments.add(cl.getTime());
				}
			}

			cl.add(Calendar.MINUTE, 30);
		}	
		return segments;
	}
	
	
	
	public  ArrayList<Person> getPersons () throws ParseException {
		
		
		ArrayList<Person> persons = new ArrayList<Person>() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			{
				add(new Person(1, "Kyle", new ArrayList<Date>() {
					{
						add(dateformat.parse("13:30"));
						add(dateformat.parse("14:30"));
						add(dateformat.parse("18:00"));
					}
				}));
				
				add(new Person(2, "Paul", new ArrayList<Date>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						add(dateformat.parse("07:00"));
						add(dateformat.parse("09:00"));
						add(dateformat.parse("13:30"));
						add(dateformat.parse("15:00"));
						add(dateformat.parse("15:30"));
					}
				}));
				
				add(new Person(3, "Alex", new ArrayList<Date>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						add(dateformat.parse("08:00"));
						add(dateformat.parse("09:30"));
						add(dateformat.parse("12:30"));
						add(dateformat.parse("15:00"));
					}
				}));
				
				
				add(new Person(4, "Luis", new ArrayList<Date>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						add(dateformat.parse("09:00"));
						add(dateformat.parse("13:30"));
						add(dateformat.parse("15:00"));
						add(dateformat.parse("15:30"));
					}
				}));
				
				add(new Person(5, "Jairo", new ArrayList<Date>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						add(dateformat.parse("08:00"));
						add(dateformat.parse("09:00"));
						add(dateformat.parse("18:00"));
					}
				}));
				
				add(new Person(6, "Sonya", new ArrayList<Date>() {
					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					{
						add(dateformat.parse("08:00"));
						add(dateformat.parse("12:30"));
						add(dateformat.parse("13:30"));
						add(dateformat.parse("15:30"));
					}
				}));
				
			}
		};
		
		
		return persons;
	}

}
