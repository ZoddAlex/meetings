package com.taskeasy.meetings.models;

import java.util.ArrayList;


public class Disposition {
	
	
	private String daySegment;

	private ArrayList<String> availablePeople;
	
	public String getDaySegment() {
		return daySegment;
	}
	public void setDaySegment(String daySegment) {
		this.daySegment = daySegment;
	}
	public ArrayList<String> getAvailablePeople() {
		return availablePeople;
	}
	public void setAvailablePeople(ArrayList<String> availablePeople) {
		this.availablePeople = availablePeople;
	}
	
	public void addToAvailablePeople(String person) {
		if(this.availablePeople == null) {
			this.availablePeople = new ArrayList<String>();
		}

		this.availablePeople.add(person);
	}
	
	public Disposition(String daySegment, ArrayList<String> availablePeople) {
		super();
		this.daySegment = daySegment;
		this.availablePeople = availablePeople;
	}
	
	public Disposition(String daySegment) {
		super();
		this.daySegment = daySegment;
	}
	public Disposition() {
		super();
	}
	@Override
	public String toString() {
		return "Disposition [daySegment=" + daySegment + ", availablePeople=" + availablePeople + "]";
	}
	
	

}
