package com.taskeasy.meetings.models;

import java.util.ArrayList;
import java.util.Date;

public class Person {
	
	private int Id;
	private String name;
	private ArrayList<Date> dates;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Date> getDates() {
		return dates;
	}
	public void setDates(ArrayList<Date> dates) {
		this.dates = dates;
	}
	public Person(int id, String name, ArrayList<Date> dates) {
		super();
		Id = id;
		this.name = name;
		this.dates = dates;
	}
	@Override
	public String toString() {
		return "Person [Id=" + Id + ", name=" + name + ", dates=" + dates + "]";
	}

	
	
	

}
